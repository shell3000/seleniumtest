import time

from selenium.webdriver.common.by import By
from selenium.webdriver.remote import  webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.firefox.options import Options as FirefoxOpts
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # driver = webdriver.WebDriver(options=Options())
    driver = webdriver.WebDriver(options=FirefoxOpts())
    try:
        print_hi('PyCharm')
        driver.get("http://www.python.org")
        assert "Python" in driver.title
        elem = driver.find_element(By.NAME, "q")
        elem.clear()
        time.sleep(2)
    except Exception as exc:
        time.sleep(10)
        print(exc)
        driver.quit()
    else:
        driver.quit()